#pragma once
#include "../transformable/transformable.h"
#include "../engine-object/engine-object.h"

#include <set>

namespace hospite
{
	/**
	* Represents interface for objects that can be placed into a level
	*/
	class IActor : public virtual IEngineObject,  public virtual Transformable
	{
	public:
		IActor();

		virtual ~IActor() = default;
		virtual bool OnInit() = 0;
		virtual void OnTick(const float timeDelta) = 0;
		virtual void OnDestroy() = 0;

		void RequestDestroy();
		void SetParent(IActor* parent);
		const bool Destroyed() const;
		const bool Initialized() const;

		Transformable GetRelativeTransform();
	private:
		bool m_initialized;
		bool m_destroyed;

		IActor * m_parent;
		std::set<IActor*> m_childreen;
	};
}