#pragma once
#include "planets.h"
#include <vector>
#include <memory>

namespace gameapp
{

	struct WorldSpecification
	{
		std::size_t m_minDistance;
		std::size_t m_connectionDistance;
		std::size_t m_planetSize;
		std::size_t m_planetCount;
		std::size_t m_maxConnections;
	};

	class World
	{
	public:
		void Destroy();
		void Generate(WorldSpecification specification);
		void Render(hospite::IRenderFrame* frame);
		WorldSpecification GetWorldInfo()const;
		float GetWorldSize()const;

		bool LoadWorldSpecification(const std::string& fileName);

		std::vector<std::unique_ptr<Planet>> m_planets;


	private:
		bool CheckConnectivity()const;
		void FindSpawnPoints();
		void GeneratePlanets();
		bool RearangePlanets();
		void ConnectPlanets();
		bool CheckDistance(const std::size_t planetId, const std::set<Planet*> & m_alreadyGeneratedPlanets)const;


		//TODO
		std::size_t m_spawnPlanet;

		WorldSpecification m_specification;
		float m_worldSize;
	};
}