#include <LuaContext.hpp>

#include "game.h"
#include "../framework/logger/logger.h"
#include "../examples/sfml-renderer/sfml-renderer.h"
#include <iostream>
#include <fstream>

namespace gameapp
{
	void LuaTest()
	{
		std::cout<< ("[C++] Lua test start\n");
		LuaContext lua;

		std::ifstream script("../sources/scripts/script.lua");
		lua.executeCode(script);
		script.close();

		auto lua_globalVariable = lua.readVariable<float>("globalVariable");
		std::cout << "[C++] Read global variable:\n" << GETINFO(lua_globalVariable) << "\n";

		auto lua_value = lua.readVariable<int>("globalTable", "value0");
		auto lua_field = lua.readVariable<std::string>("globalTable", "field1");
		auto lua_member = lua.readVariable<float>("globalTable", "member3");

		std::cout << "[C++] Read table:\n" << GETINFO(lua_value, lua_field, lua_member) <<"\n";

		const auto lua_addFnc = lua.readVariable<LuaContext::LuaFunctionCaller<int(int, int)>>("AddStuff");
		auto lua_addFncResult = lua_addFnc(7, 11);
		std::cout << "[C++] AddStuff function result:\n" << GETINFO(lua_addFncResult)<<"\n";

		std::cout << ("[C++] Lua test end\n");

	}

	bool GameApp::OnInit(hospite::AppInitContext appInitContext)
	{
		LuaTest();


		LOG_INF("Game init");
		std::srand(std::time(nullptr));

		m_camera.SetZoom(1.0f);
		RegenerateWorld();

		Player p[2];
		ShipBlueprint bl[2];
		bl[0].m_initialHp = 10;
		bl[0].m_initialWeapons.push_back(Weapon(DamageType::ENERGETICAL, 12));

		bl[1].m_initialHp = 10;
		bl[1].m_initialWeapons.push_back(Weapon(DamageType::ENERGETICAL, 3));
		bl[1].m_initialResists[DamageType::ENERGETICAL] = 5;

		p[0].RequestAdd(&bl[0], m_world.m_planets[0].get());
		p[1].RequestAdd(&bl[1], m_world.m_planets[0].get());

		p[0].Update();
		p[1].Update();
		m_world.m_planets[0]->Update();

		p[0].Update();
		p[1].Update();
		m_world.m_planets[0]->Update();

		p[0].Update();
		p[1].Update();
		m_world.m_planets[0]->Update();


		return true;
	}

	void GameApp::RegenerateWorld()
	{
		WorldSpecification specification;

		specification.m_connectionDistance = 800;
		specification.m_planetSize = 50;
		specification.m_minDistance = 2.1f * specification.m_planetSize;
		specification.m_planetCount = 50;
		specification.m_maxConnections = 2;

		ASSERT(specification.m_connectionDistance > specification.m_minDistance);
		m_world.Destroy();
		m_world.Generate(specification);

		m_camera.SetSpeed(10.0F);
		m_camera.SetInertia(0.05f);
		m_camera.SetAccelerationSpeed(1.0f);
		m_camera.SetMaxZoomSpeed(2.0f);
		m_camera.SetZoomDelta(0.02f);

		m_camera.SetBoundaries(glm::vec2(-(float)m_world.GetWorldSize() / 1.3f), glm::vec2(m_world.GetWorldSize() / 1.3f));
		m_camera.SetMaxZoom(m_world.GetWorldSize() * 700.0f);
	}

	void GameApp::OnTick(const float timeDelta)
	{
		m_camera.Update();
	}

	void GameApp::OnRender(hospite::IRenderFrame* frame)
	{

		CameraControll(frame);
		m_world.Render(frame);
	}

	void GameApp::CameraControll(hospite::IRenderFrame* frame)
	{
		auto sfmlFrame = dynamic_cast<SFMLFrame*>(frame);
		sfmlFrame->SetCameraPosition(m_camera.GetPosition().x, m_camera.GetPosition().y);
		sfmlFrame->SetCameraSize(1280 * m_camera.GetZoom(), 720 * m_camera.GetZoom());
	}

	void GameApp::OnInput(const hospite::InputEvent inputEvent)
	{
		switch (inputEvent.type)
		{
			case hospite::InputEvent::Type::KEY : 
			{
				std::cout << "KEY: " << inputEvent.data.keyCode << std::endl;
				if (inputEvent.data.keyCode == VK_RETURN)RegenerateWorld();
				if (inputEvent.data.keyCode == VK_ESCAPE)m_listener->RequestClose();
				break;
			}

			case hospite::InputEvent::Type::MOUSE_POSITION :
			{
				std::cout << "MOVED: " << inputEvent.data.position[0]<<" "<< inputEvent.data.position[1] << std::endl;
				break;
			}

			case hospite::InputEvent::Type::MOUSE_WHEEL :
			{
				std::cout << "WHEEL: " << inputEvent.data.delta << std::endl;
				if(inputEvent.data.delta > 0)m_camera.ZoomOut();
				else m_camera.ZoomIn();
				
				break;
			}

			case hospite::InputEvent::Type::MOUSE_CLICK:
			{
				std::cout << "CLICK: " << inputEvent.data.keyCode<<std::endl;
				
				break;
			}
		}
	}

	void GameApp::OnClose()
	{
		LOG_INF("App closed");
	}

}