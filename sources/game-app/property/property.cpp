#include "property.h"

namespace gameapp
{
	Player* IProperty::GetOwner() const
	{
		return m_owner;
	}
	void IProperty::SetOwner(Player* player)
	{
		m_owner = player;
	}
}