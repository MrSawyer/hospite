#include "player.h"

namespace gameapp
{
	void Player::RequestRemoval(IUnit* unit)
	{
		m_toRemove.push_back(unit);
	}

	void Player::RequestAdd(ShipBlueprint* blueprint, Planet* position)
	{
		m_toAddShips.push_back(std::make_pair(blueprint, position));
	}

	void Player::RequestAdd(StructureBlueprint* blueprint, Planet* position)
	{
		m_toAddStructures.push_back(std::make_pair(blueprint, position));
	}

	void Player::AddUnit(ShipBlueprint* blueprint, Planet* position)
	{
		m_units.insert(std::make_unique<Ship>(position, this, blueprint));
	}

	void Player::AddUnit(StructureBlueprint* blueprint, Planet* position)
	{
		m_units.insert(std::make_unique<Structure>(position, this, blueprint));
	}

	void Player::RemoveUnit(IUnit* unit)
	{
		if (unit == nullptr)return;

		auto it = std::find_if(m_units.begin(), m_units.end(), [unit](const std::unique_ptr<IUnit>& u) {return u.get() == unit; });
		if (it == m_units.end())return;

		m_units.erase(it);
	}

	void Player::Update()
	{
		for (const auto& unit : m_toRemove)RemoveUnit(unit);

		for (const auto& unit : m_toAddShips)AddUnit(unit.first, unit.second);
		for (const auto& unit : m_toAddStructures)AddUnit(unit.first, unit.second);

		m_toRemove.clear();
		m_toAddShips.clear();
		m_toAddStructures.clear();
	}
}