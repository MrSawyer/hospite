#pragma once

#include "../framework/framework.h"
#include "../app/app.h"

#include <vector>
#include <memory>
#include "../engine/engine-object/engine-object.h"
#include "world/world.h"
#include "player/player.h"
#include "camera/camera.h"

namespace gameapp
{
	class GameApp : public hospite::IApp
	{
	public:
		bool OnInit(hospite::AppInitContext appInitContext) override;
		void OnTick(const float timeDelta) override;
		void OnRender(hospite::IRenderFrame* frame) override;
		void OnInput(const hospite::InputEvent inputEvent) override;
		void OnClose() override;

	private:
		void CameraControll(hospite::IRenderFrame* frame);
		void RegenerateWorld();

		World m_world;
		Camera m_camera;
		std::vector<hospite::IEngineObject> m_objects;
		
	};
}