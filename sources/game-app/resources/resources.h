#pragma once

namespace gameapp
{
	enum class ResourceType : unsigned int
	{
		ALUMINIUM = 0,
		DEUTER = 1
	};
}