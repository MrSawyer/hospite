#pragma once
#include "../unit/unit.h"
#include "../weapon/weapon.h"

#include <functional>
namespace gameapp
{
	class Ship;

	struct ShipBlueprint : public UnitBlueprint
	{
		std::list<Weapon> m_initialWeapons;
		std::function<float(const Ship &, IUnit*, Weapon)> m_fightFunction;
	};

	class Ship : public virtual IUnit
	{
	public:
		Ship(Planet * position, Player * owner, ShipBlueprint * blueprint);
		virtual ~Ship() = default;
		
		void Attack(std::set<IUnit *> enemyUnits) const;

		void PerformPrePhase()override;
		void PerformPhase()override;
		void PerformPostPhase()override;

		bool Move(Planet* destination);

		bool IsDefender() const;
		const std::list<Weapon> GetWeapons() const;
	private:

		Planet* m_destination;
		bool m_requestMovement;

		ShipBlueprint * m_shipBlueprint;

		bool m_isDefender; // czy jest obro�c� czy atakuj�cym dany uk�ad
		std::list<Weapon> m_weapons;
	};
}