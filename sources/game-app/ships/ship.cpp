#include "ship.h"
#include "../world/planets.h"
#include "../../framework/logger/logger.h"


namespace gameapp
{
	Ship::Ship(Planet* position, Player* owner, ShipBlueprint* blueprint)
		: IUnit(position, owner, blueprint)
	{
		m_shipBlueprint = blueprint;

		m_destination = nullptr;
		m_isDefender = false;

		m_weapons = m_shipBlueprint->m_initialWeapons;
	}

	bool Ship::IsDefender() const
	{
		return m_isDefender;
	}

	const std::list<Weapon> Ship::GetWeapons() const
	{
		return m_weapons;
	}

	void Ship::Attack(std::set<IUnit*> enemyUnits) const
	{
		ASSERT_MSG(m_shipBlueprint != nullptr, "Ship without blueprint?!");

		for (auto & enemy : enemyUnits)
		{
			for (const auto& weapon : GetWeapons())
			{
				if (m_shipBlueprint->m_fightFunction)
				{
					// there is specyfic function
					float dmg = m_shipBlueprint->m_fightFunction(*this, enemy, weapon);
					enemy->AbsorbDamage(weapon.GetType(), dmg);
				}
				else
				{
					enemy->AbsorbDamage(weapon.GetType(), weapon.GetDamage());
				}
			}
		}
	}

	void Ship::PerformPrePhase()
	{
		// acctually ship does nothing during prephase
	}

	void Ship::PerformPhase()
	{
		// all units on the planet
		auto planetPlayersUnits = GetOccupiedPlanet()->GetUnits();

		// check every player
		for (auto& playerUnitsDefender : planetPlayersUnits)
		{
			// dont fight with myself XD
			if (playerUnitsDefender.first == GetOwner())continue;

			// attack all enemy units
			Attack(playerUnitsDefender.second);
		}
	}


	void Ship::PerformPostPhase()
	{
		ASSERT_MSG(GetOccupiedPlanet() != nullptr, "Ship on non existing planet?!");

		if (!m_requestMovement)
		{
			// no movement
			m_isDefender = true;
			return;
		}

		// move ship
		SetOccupiedPlanet(m_destination);
		
		m_isDefender = false;
		m_requestMovement = false;
	}

	bool Ship::Move(Planet* destination)
	{
		m_requestMovement = false;

		if (!IsReady())return false;

		if (GetOccupiedPlanet() == nullptr)
		{
			// invalid start position
			return false;
		}

		if (destination == nullptr || destination == GetOccupiedPlanet())
		{
			// no movement
			m_destination = GetOccupiedPlanet();
			return true;
		}

		if (GetOccupiedPlanet()->GetConnected().find(destination) != GetOccupiedPlanet()->GetConnected().end())
		{
			// movement
			m_destination = destination;
			m_requestMovement = true;
			return true;
		}

		// invalid destination (no movement)
		return false;
	}
}