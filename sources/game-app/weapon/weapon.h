#pragma once
#include "damage-type.h"

namespace gameapp
{
	class Weapon
	{
	public:
		Weapon(DamageType, float);

		DamageType GetType() const;
		float GetDamage() const;
	private:
		DamageType m_damageType;
		float m_value;
	};
}