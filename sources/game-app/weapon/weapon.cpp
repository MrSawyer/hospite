#include "weapon.h"

namespace gameapp
{
	Weapon::Weapon(DamageType type, float dmg)
		: m_damageType(type), m_value(dmg) {}

	DamageType Weapon::GetType() const
	{
		return m_damageType;
	}

	float Weapon::GetDamage() const
	{
		return m_value;
	}
}