#pragma once

namespace gameapp
{
	enum class DamageType : unsigned int
	{
		PHYSICAL = 0,
		ENERGETICAL = 1
	};
}