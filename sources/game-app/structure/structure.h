#pragma once
#include "../unit/unit.h"


namespace gameapp
{
	struct StructureBlueprint : public UnitBlueprint
	{
		
	};

	class Structure : public virtual IUnit
	{
	public:
		Structure(Planet* position, Player* owner, StructureBlueprint* blueprint);


		void PerformPrePhase()override;
		void PerformPhase()override;
		void PerformPostPhase()override;
	private:
		StructureBlueprint * m_blueprint;
	};
}