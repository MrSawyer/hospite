#include <Windows.h>
#define NOMINMAX
#include "camera.h"
#include <algorithm>

namespace gameapp
{

	Camera::Camera() :
		m_maxSpeed(0),
		m_inertia(0),
		m_accelerationSpeed(0),
		m_maxZoomSpeed(0),
		m_zoomDelta(0),
		m_zoomAcceleration(0),
		m_zoom(1.0f),
		m_maxZoom(1.0f)
	{
	}

	void Camera::Update()
	{
		if (GetAsyncKeyState(0x41))m_displacement.x -= m_accelerationSpeed;
		if (GetAsyncKeyState(0x44))m_displacement.x += m_accelerationSpeed;
		if (GetAsyncKeyState(0x57))m_displacement.y -= m_accelerationSpeed;
		if (GetAsyncKeyState(0x53))m_displacement.y += m_accelerationSpeed;

		m_displacement /= (1.0f + m_inertia);

		float length = glm::length(m_displacement);

		if (length < 0.01f)m_displacement = glm::vec2(0.0f, 0.0f);
		if (length > m_maxSpeed) {
			m_displacement = glm::normalize(m_displacement);
			m_displacement *= m_maxSpeed;
		}

		m_zoomAcceleration /= (1.0f + (m_inertia/2.0f));

		if (std::abs(m_zoomAcceleration) < 0.01f)m_zoomAcceleration = 0.0f;

		if (m_zoomAcceleration > m_maxZoomSpeed)
		{
			m_zoomAcceleration = m_maxZoomSpeed;
		}

		if (m_zoomAcceleration < -m_maxZoomSpeed)
		{
			m_zoomAcceleration = -m_maxZoomSpeed;
		}

		m_zoom += m_zoomAcceleration;
		m_zoom = std::max(1.0f, m_zoom);
		if (m_zoom > m_maxZoom)m_zoom = m_maxZoom;

		m_position += m_displacement;

		m_position.x = std::max(m_boundaryTopLeft.x / m_zoom, m_position.x);
		m_position.y = std::max(m_boundaryTopLeft.y / m_zoom, m_position.y);

		if (m_position.x > m_boundaryBottomRight.x / m_zoom)m_position.x = m_boundaryBottomRight.x / m_zoom;
		if (m_position.y > m_boundaryBottomRight.y / m_zoom)m_position.y = m_boundaryBottomRight.y / m_zoom;
	}

	void Camera::ZoomIn()
	{
		if (m_zoomAcceleration < 0.0f)m_zoomAcceleration = 0.0f;
		m_zoomAcceleration += m_zoomDelta;
	}

	void Camera::ZoomOut()
	{
		if (m_zoomAcceleration > 0.0f)m_zoomAcceleration = 0.0f;
		m_zoomAcceleration -= m_zoomDelta;
	}

	void Camera::SetSpeed(float speed)
	{
		m_maxSpeed = speed;
	}

	void Camera::SetAccelerationSpeed(float accelerationSpeed)
	{
		m_accelerationSpeed = accelerationSpeed;
	}

	void Camera::SetInertia(float inertia)
	{
		m_inertia = inertia;
	}

	void Camera::SetPosition(glm::vec2 position)
	{
		m_position = position;
	}

	void Camera::SetZoom(float zoom)
	{
		m_zoom = zoom;
	}

	void Camera::SetMaxZoomSpeed(float speed)
	{
		m_maxZoomSpeed = speed;
	}

	void Camera::SetZoomDelta(float delta)
	{
		m_zoomDelta = delta;
	}

	void Camera::SetMaxZoom(float maxZoom)
	{
		m_maxZoom = maxZoom;
	}

	void Camera::SetBoundaries(glm::vec2 topLeft, glm::vec2 bottomRight)
	{
		m_boundaryTopLeft = topLeft;
		m_boundaryBottomRight = bottomRight;
	}

	glm::vec2 Camera::GetDisplacement() const
	{
		return m_displacement;
	}

	glm::vec2 Camera::GetPosition() const
	{
		return m_position;
	}
	float Camera::GetZoom() const
	{
		return m_zoom;
	}
}