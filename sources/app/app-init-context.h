#pragma once
#include "../framework/window/resolution.h"

namespace hospite
{
	struct AppInitContext
	{
		Resolution m_resolution;
	};
}