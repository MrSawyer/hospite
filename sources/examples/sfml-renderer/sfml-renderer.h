#pragma once
#include <SFML/Graphics.hpp>
#include "../../framework/framework.h"

// --- Render frame definition
class SFMLFrame : public hospite::IRenderFrame
{
public:
	void Reset() override;
	void DisplayObjects(sf::RenderWindow& window)const;
	void AddObject(std::shared_ptr<sf::Drawable> obj);
	void SetCameraPosition(float x, float y);
	void SetCameraSize(float width, float height);
	void MoveCamera(float dx, float dy);
private:
	std::vector<std::shared_ptr<sf::Drawable>> m_objects;
	sf::View m_camera;
};

// --- Renderer definition
class SFMLRenderer : public hospite::IRenderer
{
public:
	bool Init(hospite::Window* window) override;
	void Render() override;

private:
	std::unique_ptr<sf::RenderWindow> m_sfml;
	SFMLFrame* m_frameCasted;
};
