#include "sfml-renderer.h"

void SFMLFrame::Reset()
{
	m_objects.clear();
}

void SFMLFrame::SetCameraPosition(float x, float y)
{
	m_camera.setCenter(x, y);
}

void SFMLFrame::SetCameraSize(float width, float height)
{
	m_camera.setSize(width, height);
}

void SFMLFrame::MoveCamera(float dx, float dy)
{
	m_camera.move(dx, dy);
}

void SFMLFrame::DisplayObjects(sf::RenderWindow& window)const
{
	window.setView(m_camera);
	for (auto& obj : m_objects)
	{
		window.draw(*obj);
	}
}

void SFMLFrame::AddObject(std::shared_ptr<sf::Drawable> obj)
{
	m_objects.push_back(obj);
}
	//-------

bool SFMLRenderer::Init(hospite::Window* window)
{
	if (window == nullptr)
		return false;

	m_sfml = std::make_unique<sf::RenderWindow>(window->CreateViewBuffer());

	m_frame = std::make_unique<SFMLFrame>();
	m_frameCasted = dynamic_cast<SFMLFrame*>(m_frame.get());

	return true;
}

void SFMLRenderer::Render()
{
	m_sfml->clear();
	m_frameCasted->DisplayObjects(*m_sfml);
	m_sfml->display();
}
