#pragma once
#include <SFML/Graphics.hpp>
#include "sfml-renderer/sfml-renderer.h"
#include "../framework/framework.h"
#include "../app/app.h"

//-------------- App definition
class App : public hospite::IApp
{
public:
	bool OnInit(hospite::AppInitContext appInitContext) override
	{
		shape.setFillColor(sf::Color::Green);
		m_time = 0;
		m_direction = true;
		return  true;
	}

	void OnTick(const float timeDelta) override
	{
		if (m_time <= 0.0f)m_direction = true;
		if (m_time > 100.0f)m_direction = false;


		if (m_direction)m_time += 50.0f * timeDelta;
		else m_time -= 50.0f * timeDelta;
		
		shape.setRadius(m_time);
	}

	void OnRender(hospite::IRenderFrame* frame) override
	{
		dynamic_cast<SFMLFrame*>(frame)->AddObject(&shape);
	}

	void OnInput(const hospite::InputEvent inputEvent) override
	{

	}
	void OnClose() override
	{};

private:
	sf::CircleShape shape;
	bool m_direction;
	float m_time;
};