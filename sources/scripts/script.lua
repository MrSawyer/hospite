function AddStuff(a, b)
	print("[LUA] AddStuff("..a..","..b..")")
	return a + b
end

print("[LUA] Script start")

globalVariable = 17 + math.sin(25)
print("[LUA] globalVariable: "..globalVariable)

globalTable =
			{ 
				value0 = 7,
				field1 = "lorem ipsum",
				member3 = 98.74
			}

print("[LUA] Script end")