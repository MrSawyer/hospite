#pragma once

namespace hospite
{
	struct InputEvent
	{
		enum class Type
		{
			KEY, MOUSE_WHEEL, MOUSE_POSITION, MOUSE_CLICK
		};

		Type type;
		
		union InputData
		{
			int position[2];
			int delta;
			int keyCode;
		};

		InputData data;
	};
}