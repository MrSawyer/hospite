#pragma once

namespace hospite
{
	struct Resolution
	{
		unsigned int WIDTH;
		unsigned int HEIGHT;
	};
}