#pragma once

namespace hospite
{
	class IRenderFrame
	{
	public:
		virtual ~IRenderFrame() {};
		virtual void Reset() = 0;
	};
}